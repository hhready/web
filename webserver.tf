provider "yandex" {}

resource "yandex_compute_instance" "my_webserver" {
  boot_disk {
    initialize_params {
      image_id = "fd8vmcue7aajpmeo39kk"
      size = 5
    }
  }
  metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
  network_interface {
    subnet_id = "default/default-ru-central1-b"
  }
  resources {
    cores = 2
    memory = 2
    core_fraction = 5
  }
}
provider "yandex" {
  token     = "<OAuth или статический ключ сервисного аккаунта>"
  folder_id = "<идентификатор каталога>"
  zone      = "ru-central1-a"
}

resource "yandex_vpc_security_group" "test-sg" {
  name        = "Test security group"
  description = "Description for security group"
  network_id  = "<Идентификатор сети>"

  ingress {
    protocol       = "TCP"
    description    = "Rule description 1"
    v4_cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24"]
    port           = 8080
  }

  egress {
    protocol       = "ANY"
    description    = "Rule description 2"
    v4_cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24"]
    from_port      = 8090
    to_port        = 8099
  }
}